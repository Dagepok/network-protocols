﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Pop3
{
    public class Content
    {
        private static readonly Dictionary<string, string> Types = new Dictionary<string, string>
        {
            {"Text/Plain", ".txt"},
            {"Image/Jpeg", ".jpeg"}
        };
        public string Type { get; set; }
        public byte[] Data { get; set; }
        public string Name { get; set; }

        public Content(string type, byte[] data, string name = "data")
        {
            Type = Types[type];
            Data = data;
            Name = name;
        }
    }
}