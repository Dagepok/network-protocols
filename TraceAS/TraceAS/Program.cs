﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TraceAS
{
    internal class Program
    {


        public string GetIP(string hostName)
        {
            var ipHost = hostName;
            if (Regex.IsMatch(ipHost, @"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b")) return ipHost;
            try
            {
                ipHost = Dns.GetHostEntry(hostName).AddressList[0].ToString();
            }
            catch (SocketException e)
            {
                Console.WriteLine("Can't find '" + hostName + "'." + e.Message);
                Environment.Exit(0);
            }
            return ipHost;
        }

        private static void Main(string[] args)
        {
            Console.WriteLine("{0,1} {1,5} {2,10} {3,40} {4,40}\r\n", "№", "IpAddress", "AS", "Host", "Country");
          //  TraceRoute.GetTraceRoute("tni.mil.id");
            TraceRoute.GetTraceRoute("www.tni.mil.id");
        }

    }
}
