﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.NetworkInformation;
using System.Text;
using System.Net;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TraceAS
{
    public class TraceRoute
    {
        private static readonly string MyIp = new WebClient().DownloadString("https://api.ipify.org");
        private static byte[] Buffer => Encoding.ASCII.GetBytes("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        private static AsInfo MyAS = GetAsInfo(MyIp);
        private static bool IsInMyAss = true;


        public static IEnumerable<IPAddress> GetTraceRoute(string hostNameOrAddress)
        {
            return GetTraceRoute(hostNameOrAddress, 1);
        }


        private static AsInfo GetAsInfo(string ip)
        {
            if (ip.StartsWith("10.") || ip.StartsWith("172.") || ip.StartsWith("192.168."))
            {
                return IsInMyAss ? MyAS : new AsInfo("Undefined", "Undefined", "Undefined");
            }
            IsInMyAss = false;
            var request =
                    WebRequest.Create("https://stat.ripe.net/data/prefix-overview/data.json?resource=" + ip + "&lod=2");
            // ReSharper disable once AssignNullToNotNullAttribute
            var reader = new JsonTextReader(new StreamReader(request.GetResponse().GetResponseStream()));
            var obj = JObject.Load(reader);
            var host = obj["data"]["asns"].First["holder"].ToString().Split(',');
            var number = obj["data"]["asns"].First["asn"].ToString();
            return new AsInfo(number, host[0], host[host.Length - 1]);
        }


        private static IEnumerable<IPAddress> GetTraceRoute(string hostNameOrAddress, int ttl)
        {
            var pinger = new Ping();
            var pingerOptions = new PingOptions(ttl, true);
            const int timeout = 3000;

            var reply = pinger.Send(hostNameOrAddress, timeout, Buffer, pingerOptions);
            Thread.Sleep(1000);
            var result = new List<IPAddress>();
            if (reply.Status == IPStatus.Success)
            {
                result.Add(reply.Address);
                GetAsAndWrite(reply, ttl);
            }
            else if (reply.Status == IPStatus.TtlExpired)
            {
                result.Add(reply.Address);
                GetAsAndWrite(reply, ttl);
                result.AddRange(GetTraceRoute(hostNameOrAddress, ++ttl));
            }
            else if (reply.Status == IPStatus.TimedOut)
            {
                Console.WriteLine("{0,1} {1,10}\n", ttl, reply.Status);
                result.AddRange(GetTraceRoute(hostNameOrAddress, ++ttl));
            }
            return result;
        }

        private static void GetAsAndWrite(PingReply reply, int ttl)
        {
            try
            {
                var AS = GetAsInfo(reply.Address.ToString());
                Console.WriteLine("{0,1} {1,5} {2,10} {3,40} {4,40}\n", ttl, reply.Address, AS.Number, AS.Host,
                    AS.Country);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0,1} {1,10} RIR can't get info\n", ttl, reply.Status);
            }
        }
    }
    internal class AsInfo
    {
        internal string Number { get; }
        internal string Host { get; }
        internal string Country { get; set; }

        internal AsInfo(string number, string host, string country)
        {
            Number = number;
            Host = host;
            Country = country;
        }
    }
}
